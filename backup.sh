#!/bin/bash
function Draw {
	clear
	echo "┌────────────────────────────────────────────────────────────┐"
	echo "─                                                            ─"
	echo "─                Website / Server Downloader                 ─"
	echo "─                      By Sam Grew                           ─"
	echo "─                                                            ─"
	echo "─                       Git Clone                            ─"
	echo "─    https://gitlab.com/AceXintense/bash-download-websites   ─"
	echo "─                                                            ─"
	echo "└────────────────────────────────────────────────────────────┘"
}
function Create {
	echo "┌────────────────────────────────────────────────────────────┐"
	echo "- Convert Files from PHP to HTML? [Y/N]                      -"
	echo "└────────────────────────────────────────────────────────────┘"
	read Convert
	Draw
	echo "┌────────────────────────────────────────────────────────────┐"
	echo "- Download Files in Background? [Y/N]                        -"
	echo "└────────────────────────────────────────────────────────────┘"
	read Background
	Draw
	echo "┌────────────────────────────────────────────────────────────┐"
	echo "- Limit File Download Speed? [Y/N]                           -"
	echo "└────────────────────────────────────────────────────────────┘"
	read Limit
	Draw
	if [ $Background == "Y" ] || [ $Background == "y" ]; then
		if [ $Limit == "Y" ] || [ $Limit == "y" ]; then
			echo "┌────────────────────────────────────────────────────────────┐"
			echo "- Input Download Speed [1, 1000]                             -"
			echo "└────────────────────────────────────────────────────────────┘"
			read DownloadSpeed
			Draw
			echo "┌────────────────────────────────────────────────────────────┐"
			echo "- Input Datatype [K/M]                                       -"
			echo "└────────────────────────────────────────────────────────────┘"
			read Datatype
			Draw
			if [ $Datatype == "K" ] || [ $Datatype == "k" ] || [ $Datatype == "M" ] || [ $Datatype == "m" ]; then
				if [ $Convert == "Y" ] || [ $Convert == "y" ]; then
					wget -r -k -b --limit-rate=$DownloadSpeed$Datatype $URL
					echo ./
					find ./ -depth -name "*.php" -exec sh -c 'mv "$1" "${1%.php}.html"' _ {} \;
					open ./$URL
				elif [ $Convert == "N" ] || [ $Convert == "n" ]; then
					wget -r -k -b --limit-rate=$DownloadSpeed$Datatype $URL
					open ./$URL
				fi
			fi
		elif [ $Limit == "N" ] || [ $Limit == "n" ]; then
			if [ $Convert == "Y" ] || [ $Convert == "y" ]; then
				wget -r -b -k $URL
				echo ./
				find ./ -depth -name "*.php" -exec sh -c 'mv "$1" "${1%.php}.html"' _ {} \;
				open ./$URL
			elif [ $Convert == "N" ] || [ $Convert == "n" ]; then
				wget -r -b -k $URL
				open ./$URL
			fi
		fi
	elif [ $Background == "N" ] || [ $Background == "n" ]; then
		if [ $Limit == "Y" ] || [ $Limit == "y" ]; then
			if [ $Datatype == "K" ] || [ $Datatype == "k" ] || [ $Datatype == "M" ] || [ $Datatype == "m" ]; then
				if [ $Convert == "Y" ] || [ $Convert == "y" ]; then
					wget -r -k --limit-rate=$DownloadSpeed$Datatype $URL
					echo ./
					find ./ -depth -name "*.php" -exec sh -c 'mv "$1" "${1%.php}.html"' _ {} \;
					open ./$URL
				elif [ $Convert == "N" ] || [ $Convert == "n" ]; then
					wget -r -k --limit-rate=$DownloadSpeed$Datatype $URL
					open ./$URL
				fi
			fi
		elif [ $Limit == "N" ] || [ $Limit == "n" ]; then
			if [ $Convert == "Y" ] || [ $Convert == "y" ]; then
				wget -r -k $URL
				echo ./
				find ./ -depth -name "*.php" -exec sh -c 'mv "$1" "${1%.php}.html"' _ {} \;
				open ./$URL
			elif [ $Convert == "N" ] || [ $Convert == "n" ]; then
				wget -r -k $URL
				open ./$URL
			fi
		fi
	fi
}
Path=$(cd "$(dirname "$0")"; pwd)
cd $Path
Draw
echo "┌────────────────────────────────────────────────────────────┐"
echo "- Website / Server URL Or Enter Q to Exit                    -"
echo "└────────────────────────────────────────────────────────────┘"
read  URL
Draw
if [ $URL == "Q" ] || [ $URL == "q" ]; then
	clear
	exit
fi
if [ ! -d "Backup" ]; then
	mkdir Backup	
fi
if [ -d "Backup" ]; then
	cd ./Backup
	if [ ! -d $URL ]; then
		Create
	elif [ -d $URL ]; then
		echo "┌────────────────────────────────────────────────────────────┐"
		echo "- Folder Already Downloaded Overwrite Folder? [Y/N]          -"
		echo "└────────────────────────────────────────────────────────────┘"
		read Overwrite
		Draw
		rm -r ./$URL
		mkdir $URL
		if [ $Overwrite == "Y" ] || [ $Overwrite == "y" ]; then
			Create
		elif [ $Overwrite == "N" ] || [ $Overwrite == "n" ]; then
			cd $Path
			Draw
			./$0
		fi
	fi
fi
echo "┌────────────────────────────────────────────────────────────┐"
echo "- Download Another Server / Website? [Y/N]                   -"
echo "└────────────────────────────────────────────────────────────┘"
read bool
Draw
if [ $bool == "Y" ] || [ $bool == "y" ]; then
	cd $Path
	Draw
	./$0
elif [ $bool == "N" ] || [ $bool == "n" ]; then
	clear
	exit
fi